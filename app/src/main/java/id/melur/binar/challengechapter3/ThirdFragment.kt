package id.melur.binar.challengechapter3

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentContainer
import androidx.fragment.app.setFragmentResultListener
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter3.databinding.FragmentSecondBinding
import id.melur.binar.challengechapter3.databinding.FragmentThirdBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class ThirdFragment : Fragment() {

    private var _binding: FragmentThirdBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentThirdBinding.inflate(inflater, container, false)
        return binding.root

    }

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setFragmentResultListener(FourthFragment.REQUEST_KEY) { _, bundle ->
            val person = bundle.getParcelable<Person>(FourthFragment.EXTRA_PERSON) as Person
            val gg = ganjilGenap(person.usia)
            binding.textView3.text = "${person.nama} \n ${person.usia}$gg \n ${person.alamat} \n ${person.pekerjaan}"
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        !
        val person = ThirdFragmentArgs.fromBundle(arguments as Bundle).keyPerson
        binding.textView3.text = person.nama

        binding.button3.setOnClickListener {
            val toFourthFragment = ThirdFragmentDirections.actionThirdFragmentToFourthFragment()
            it.findNavController().navigate(toFourthFragment)
        }
    }

    fun ganjilGenap(value: String): String {

        val ganjil = ", bernilai Ganjil"
        val genap = ", bernilai Ganjil"
        val y = value.toInt()

        if (y%2 == 0){
            return ganjil
        } else {
            return genap
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}
