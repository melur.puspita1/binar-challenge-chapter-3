package id.melur.binar.challengechapter3

import android.os.Parcel
import android.os.Parcelable
import kotlinx.parcelize.Parcelize
import java.io.Serializable

@Parcelize
data class Person(

    val nama: String? = null,
    val usia: String = "",
    val alamat: String? = null,
    val pekerjaan: String? = null

) : Parcelable
