package id.melur.binar.challengechapter3

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.fragment.app.FragmentContainer
import androidx.fragment.app.setFragmentResult
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import id.melur.binar.challengechapter3.databinding.FragmentFourthBinding
import id.melur.binar.challengechapter3.databinding.FragmentSecondBinding

/**
 * A simple [Fragment] subclass as the second destination in the navigation.
 */
class FourthFragment : Fragment() {

    private var _binding: FragmentFourthBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        _binding = FragmentFourthBinding.inflate(inflater, container, false)
        return binding.root

    }

    private val backPressedCallback = object : OnBackPressedCallback(true) {
        override fun handleOnBackPressed() {
            setResult()
            requireActivity().findNavController(R.id.konten).popBackStack()
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().onBackPressedDispatcher.addCallback(this, backPressedCallback)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentFourthBinding.bind(view)

        binding.button4.setOnClickListener {
            val usia = binding.inputusia.text.toString()
            val alamat = binding.inputalamat.text.toString()
            val pekerjaan = binding.inputkerja.text.toString()

            val person = Person(
                usia = usia,
                alamat = alamat,
                pekerjaan = pekerjaan
            )

            val bundle = Bundle().apply {
                putParcelable(EXTRA_PERSON, person)
            }

            setFragmentResult(REQUEST_KEY, bundle)

            it.findNavController().popBackStack()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun setResult() {
        val usia = binding.inputusia.text.toString()
        val alamat = binding.inputalamat.text.toString()
        val pekerjaan = binding.inputkerja.text.toString()

        val person = Person(
            usia = usia,
            alamat = alamat,
            pekerjaan = pekerjaan
        )

        val bundle = Bundle().apply {
            putParcelable(EXTRA_PERSON, person)
        }

        setFragmentResult(REQUEST_KEY, bundle)
    }

    companion object {
        const val EXTRA_PERSON = "extra_person"
        const val REQUEST_KEY = "request_key"
    }
}

